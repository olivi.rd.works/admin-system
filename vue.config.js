const webpack = require('webpack')
const path = require('path') // -- 引用node處理路徑模組

/** 組合路徑 */
function resolvePath(dir) {
  return path.join(__dirname, './', dir)
}

/** 環境設定 */
module.exports = {
  publicPath: './', // -- 設定根目錄
  outputDir: 'dist', // -- build專案時輸出的目錄位置
  devServer: { // -- dev環境伺服器設定
    port: 8080,
    proxy: { // -- 配置Axios(proxyTable)跨域代理方法
      '/api': {
        target: 'http://localhost:3000', // --需要請求的地址
        changeOrigin: true, // --是否支持跨網域
        ws: true,
        pathRewrite: {
          '^/api': '/', // --替換target中的請求地址，例如請求的時候把'/{檔名}'換成'/mock/{檔名}'
        },
      },
    },
  },
  /** ----- chainWebpack：修改webpack設定，且為直接針對webpack設定做修改 -----*/
  chainWebpack: config => {
    // for webpack seeting
  },
  /** ----- 修改webpack設定，或回傳webpack-merge合併後的設置項目 -----*/
  configureWebpack: {
    resolve: {
      extensions: ['.js'],
      // -- 路徑別名
      alias: {
        '@': resolvePath('src'),
        '@css': resolvePath('src/assets/sass'),
        '@img': resolvePath('src/assets/image'),
        '@js': resolvePath('src/script'),
        '@component': resolvePath('src/components'),
        '@layout': resolvePath('src/layout'),
        '@view': resolvePath('src/views'),
        '@vuex': resolvePath('src/vuex'),
        jquery: 'jquery',
      },
    },
    plugins: [
      // -- ProvidePlugin：無須import或require，而是自動加載套件引用。
      new webpack.ProvidePlugin({
        jQuery: 'jquery',
        $: 'jquery',
      }),
    ],
  },
}
