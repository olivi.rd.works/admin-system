const path = require("path");

module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 6,
    sourceType: "module",
    ecmaFeatures: {
      "jsx": true,
      "modules": true,
      "experimentalObjectRestSpread": true
    }
  },
  env: {
    node: true,
    jquery: true,
    browser: true,
    es6: true
  },
  extends: [
    "plugin:vue/essential",
    "@vue/airbnb",
  ],
  settings: {
    "import/resolver": {
      webpack: {
        config: {
          resolve: {
            alias: {
              "@": path.resolve("./src"),
              '@css': path.resolve('./src/assets/sass'),
              '@img': path.resolve('./src/assets/image'),
              '@js': path.resolve('./src/script'),
              '@component': path.resolve('./src/components'),
              '@layout': path.resolve('./src/layout'),
              '@view': path.resolve('./src/views'),
              '@vuex': path.resolve('./src/vuex'),
            },
            extensions: [".js", ".vue"]
          }
        }
      }
    },
    // "import/extensions": [".js", ".vue"]
  },
  rules: {
    // "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    // "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    'indent': 'error',
    'no-console': 'off',
    'semi': 'off',
    'comma-dangle': ['error', 'never'],
    'arrow-parens': ['error', 'as-needed'],
    'prefer-template': 'off',
    'prefer-destructuring': 'off',
    'import/first': 'off',
    'linebreak-style': 'off',
    'import/no-extraneous-dependencies': 'off',
    'no-multi-spaces': 'off',
    'no-useless-escape': 'off',
    'quote-props': 'off',
    'max-len': 'warn',
    'class-methods-use-this': 'off',
    'no-underscore-dangle': ['error', {
      allowAfterThis: true
    }],
    'no-plusplus': 'off',
    'no-shadow': ['error', {
      allow: ['state']
    }],
    'no-prototype-builtins': 'off',
    'arrow-body-style': 'off',
    'no-restricted-syntax': 'off',
    'guard-for-in': 'off',
    'no-case-declarations': 'off',
    'import/extensions': ['error', 'always', {
      js: 'never',
      vue: 'never',
      svg: 'nerver'
    }],
    'no-param-reassign': ['error', {
      props: true,
      ignorePropertyModificationsFor: [
        'state',    // for vuex state
        'e',        // for e.returnvalue
        'request',
        'config',   // for request config settings
        'Vue',      // for vue plugins define
        'editor'    // for froala
      ]
    }],
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "import/extensions": [2, "never", { 
      "vue": "never" 
    }],
    "comma-dangle": ["error", {
      "arrays": "always-multiline",
      "objects": "always-multiline",
      "imports": "never",
      "exports": "never",
      "functions": "ignore"
    }],
    "semi": ["error", "never"],
    "func-names": [0, "always"],
    // eslint报错Expected linebreaks to be "LF" but found "CRLF"
    // 因git為unix系統的換行字符與本機的windos不符合導致報錯所做設定
    "linebreak-style": "off",
  },
};
