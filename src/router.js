import Vue from 'vue'
import Router from 'vue-router'

import { links } from '@/mock/data/menu'

Vue.use(Router)

function getRouter() {
  const routes = links.result.map(item => {
    const data = {
      path: item.path,
      name: item.name,
      component: () => import(`@view/${item.componentName}`)
    }
    return data
  })
  return routes
}

// console.log(getRouter())

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  linkExactActiveClass: 'active',
  routes: getRouter()
})
