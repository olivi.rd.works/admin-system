const moment = require('moment')

export function timeFormat(time, format) {
  return moment(time).format(format)
}

export function message(msg) {
  return alert(msg)
}

export default { timeFormat, message }
