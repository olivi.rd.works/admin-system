import axios from 'axios'

const instance = axios.create({
  timeout: 10000,
  baseURL: 'http://localhost:3000/',
  // headers: { 'X-Custom-Header': 'foobar' },
})

// 添加请求拦截器
axios.interceptors.request.use(config => config.data, error => Promise.reject(error))

// 添加响应拦截器
axios.interceptors.response.use(response => response.data, error => Promise.reject(error))

export default instance
