function links(config) {
  // 模擬數據
  const result = {
    status: '0',
    result: [
      {
        path: '/',
        name: 'Home',
        componentName: 'Home',
      },
      {
        path: '/TabCompanent',
        name: 'Tab',
        componentName: 'TabCompanent',
      },
    ],
  }
  return result
}

module.exports = {
  links: links(),
}
