const Mock = require('mockjs')

const apiUser = require('./data/user.js')
const apiMenu = require('./data/menu.js')

module.exports = {
  login: apiUser.login,
  menu: apiMenu.links,
}
